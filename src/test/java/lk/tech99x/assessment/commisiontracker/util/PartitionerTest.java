package lk.tech99x.assessment.commisiontracker.util;

import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.AddressBook;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruit;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.SkillsGroup;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class PartitionerTest {

    private List<Recruit> recruits = new ArrayList<>();

    private Partitioner partitioner = new Partitioner();

    @Before
    public void setUp() {
        for (int i = 0; i < 37; i++) {
            recruits.add(getRandomRecruit(SkillsGroup.MASONRY));
        }

        for (int i = 0; i < 14; i++) {
            recruits.add(getRandomRecruit(SkillsGroup.CARPENTRY));
        }
    }

    private Recruit getRandomRecruit(SkillsGroup skillsGroup) {
        Personal personal = new Personal();
        personal.setFirstName(RandomStringUtils.randomAlphabetic(6));
        personal.setLastName(RandomStringUtils.randomAlphabetic(6));
        personal.setDateOfBirth(LocalDate.now());

        AddressBook addressBook = AddressBook.builder()
            .email(RandomStringUtils.randomAlphabetic(5) + "@noname.com")
            .addressLine1(RandomStringUtils.randomAlphanumeric(20))
            .mobilePhone(RandomStringUtils.randomNumeric(10))
            .build();

        Recruit recruit = Recruit.builder()
            .nic(RandomStringUtils.randomNumeric(10))
            .skillsGroup(skillsGroup)
            .approved(true)
            .personal(personal)
            .addressBook(addressBook)
            .build();
        return recruit;
    }

    @Test
    public void partitionGroups() throws Exception {


        assertThat(partitioner.partitionGroups(recruits)).hasSize(9);
        assertThat(partitioner.partitionGroups(recruits)
            .stream().filter(g -> g.getSkillsGroup().equals(SkillsGroup.MASONRY))).hasSize(7);
        assertThat(partitioner.partitionGroups(recruits)
            .stream().filter(g -> g.getSkillsGroup().equals(SkillsGroup.CARPENTRY))).hasSize(2);
    }

}
