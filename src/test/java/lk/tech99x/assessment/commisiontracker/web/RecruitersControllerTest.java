package lk.tech99x.assessment.commisiontracker.web;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.AddressBook;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruiter;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lk.tech99x.assessment.commisiontracker.service.business.RecruitersManagementService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(RecruitersController.class)
//@ContextConfiguration( classes = BaseTestConfig.class)
public class RecruitersControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private RecruitersManagementService recruitersManagementService;

    private Set<Recruiter> recruiters = new HashSet<>();

    @Before
    public void setUp() {

        Personal personal1 = Personal.builder().firstName("recruiter1").lastName("isgood").build();
        AddressBook addressBook1 = AddressBook.builder().id(1).email("recruiter1@gmail.com").build();
        Recruiter recruiter1 =
            Recruiter.builder()
                .id(1)
                .personal(personal1)
                .addressBook(addressBook1).build();

        Personal personal2 = Personal.builder().firstName("recruiter2").lastName("isok").build();
        AddressBook addressBook2 = AddressBook.builder().id(2).email("recruiter2@gmail.com").build();
        Recruiter recruiter2 =
            Recruiter.builder()
                .id(2)
                .personal(personal2)
                .addressBook(addressBook2).build();

        recruiters.add(recruiter1);
        recruiters.add(recruiter2);
    }


    @Test
    public void getRecruiters_shouldReturnRecruitersPage() throws Exception {


        given(recruitersManagementService.findAll()).willReturn(recruiters);

        mockMvc.perform(MockMvcRequestBuilders.get("/recruiters/all"))
            .andExpect(status().isOk())
            .andExpect(view().name("recruiters/all"))
            .andExpect(model().attribute("recruiters", hasSize(2)))
            .andExpect(model().attribute("recruiters", hasItem(
                allOf(
                    hasProperty("id", is(1)),
                    hasProperty("personal", hasProperty("firstName", is("recruiter1"))),
                    hasProperty("personal", hasProperty("lastName", is("isgood"))),
                    hasProperty("addressBook", hasProperty("email", is("recruiter1@gmail.com")))

                )
            )))
            .andExpect(model().attribute("recruiters", hasItem(
                allOf(
                    hasProperty("id", is(2)),
                    hasProperty("personal", hasProperty("firstName", is("recruiter2"))),
                    hasProperty("personal", hasProperty("lastName", is("isok"))),
                    hasProperty("addressBook", hasProperty("email", is("recruiter2@gmail.com")))

                )
            )));

    }

    @Test
    public void getRecruiterDetails_shouldReturnRecruiterDetailsPage() throws Exception {
    }

    public void getCommissionDetails_shouldReturnRecruiterCommissionDetailsPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/recruiters/harshaa/commission"))
            .andExpect(status().isOk())
            .andExpect(view().name("recruiters/commission"));
    }

    @Autowired
    public void setMockMvc(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }
}
