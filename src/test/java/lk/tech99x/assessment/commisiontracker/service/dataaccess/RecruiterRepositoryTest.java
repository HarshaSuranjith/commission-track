package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.AddressBook;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruit;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruiter;
import lk.tech99x.assessment.commisiontracker.domain.Status;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RecruiterRepositoryTest {


    private RecruiterRepository recruiterRepository;

    private TestEntityManager entityManager;

    @Test
    public void save_shouldPersistAndReturnWithId() {

        AddressBook hansAddressBook = AddressBook.builder()
            .email("hansolo@starwars.com").addressLine1("coruscant").mobilePhone("123456789").build();
        hansAddressBook.setStatus(Status.ACTIVE);
        Personal hansPersonalDetails = Personal.builder()
            .firstName("Han").lastName("Solo").build();

        Recruiter recruiter = Recruiter.builder().recruiterCode("hansolo").addressBook(hansAddressBook).personal(hansPersonalDetails).build();
        recruiter.setStatus(Status.ACTIVE);
        recruiter = recruiterRepository.saveAndFlush(recruiter);

        assertThat(recruiter, hasProperty("id"));

    }

    @Test
    public void delete_shouldDeleteAndMarkEntityAsDelete() {
        AddressBook hansAddressBook = AddressBook.builder()
            .email("hansolo@starwars.com").addressLine1("coruscant").mobilePhone("123456789").build();
        hansAddressBook.setStatus(Status.ACTIVE);
        Personal hansPersonalDetails = Personal.builder()
            .firstName("Han").lastName("Solo").build();
        Recruiter hansolo = Recruiter.builder().recruiterCode("hansolo").addressBook(hansAddressBook).personal(hansPersonalDetails).build();
        hansolo.setStatus(Status.ACTIVE);

        Recruiter savedHanSolo = this.entityManager.persistFlushFind(hansolo);
        assertThat(savedHanSolo, hasProperty("addressBook"));
        assertThat(savedHanSolo, hasProperty("addressBook", hasProperty("id")));

        this.recruiterRepository.delete(savedHanSolo);

        AddressBook deletedHanSoloAddress = this.entityManager.find(AddressBook.class, savedHanSolo.getAddressBookId());

        assertThat(deletedHanSoloAddress, is(nullValue()));

    }


    @Test
    public void findAllRecruitsForMonthAndRecruiter_shouldReturnRecruitsForMonth() throws Exception {

        Integer lengthOfMonth =  LocalDate.now().lengthOfMonth();

        Set<Recruit> recruits = this.recruiterRepository.findAllRecruitsForMonthAndRecruiter(
            LocalDateTime.now().withDayOfMonth(1),
            LocalDateTime.now().withDayOfMonth(lengthOfMonth),
            "adamjones");
        assertThat(recruits, hasSize(5));

    }

    @Autowired
    public void setRecruiterRepository(RecruiterRepository recruiterRepository) {
        this.recruiterRepository = recruiterRepository;
    }

    @Autowired
    public void setEntityManager(TestEntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
