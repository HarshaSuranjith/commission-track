package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Set;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RecruitmentRequestRepositoryTest {


    private RecruitmentRequestRepository recruitmentRequestRepository;

    private TestEntityManager entityManager;

    @Test
    public void findBySubmissionDateBetween() throws Exception {

        Set<RecruitmentRequest> recruitmentRequests = this.recruitmentRequestRepository.findBySubmissionDateBetween(LocalDateTime.now().withDayOfMonth(1), LocalDateTime.now().withDayOfMonth(30));

    }

    @Autowired
    public void setEntityManager(TestEntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Autowired
    public void setRecruitmentRequestRepository(RecruitmentRequestRepository recruitmentRequestRepository) {
        this.recruitmentRequestRepository = recruitmentRequestRepository;
    }
}
