//package lk.tech99x.assessment.commisiontracker.config;
//
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
//public class BaseTestConfig {
//
//    @Configuration
//    static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//        @Override
//        protected void configure(HttpSecurity http) throws Exception {
//            http.
//                httpBasic().disable()
//                .authorizeRequests()
//                .anyRequest()
//                .permitAll();
//        }
//    }
//
//    @Configuration
//    static class WebMvcConfiguration implements WebMvcConfigurer {
//        @Override
//        public void configureViewResolvers(ViewResolverRegistry registry) {
//            registry
//                .viewResolver();
//        }
//    }
//
//}
