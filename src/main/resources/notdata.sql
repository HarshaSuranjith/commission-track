DELETE FROM USER_ROLES;
DELETE FROM USER;
DELETE FROM ROLE;
DELETE FROM RECRUIT;
DELETE FROM RECRUITMENT_REQUEST;
DELETE FROM RECRUITER;
DELETE FROM ADDRESS_BOOK;

INSERT INTO ADDRESS_BOOK (id, created_by, created_date, last_modified_by, last_modified_date, status, version, address_line1, email, mobile_phone)
VALUES
    (1, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'first street, town one',
     'adamjones@gmail.com', '987654321'),
    (2, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'second street, town two',
     'markhamill@gmail.com', '123456789'),
    (3, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'third street, town three',
     'markwahlberg@gmail.com', '678912345'),
    (10, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'Kamino',
     'bobbafett@gmail.com', '135789002'),
    (11, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'Concord Dawn',
     'jangofett@gmail.com', '135755002'),
    (12, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'Coruscant',
     'imperialmaso@gmail.com', '135755002'),
    (13, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'Dantooine',
     'rebelmason@gmail.com', '135755002'),
    (14, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'Siilur',
     'rebelmason@gmail.com', '135755002');

INSERT INTO RECRUITER (id, created_by, created_date, last_modified_by, last_modified_date, status, version, address_book_id, first_name, last_name, code, nic)
VALUES
    (1, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 1, 'Adam', 'Jones',
        'adamjones', 'AAABBBCCCV'),
    (2, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 2, 'Mark', 'Hamill',
        'markhamill', 'AABBCCDDV'),
    (3, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 3, 'Mark', 'Wahlberg',
        'markwahlberg', 'ABCDABCDV');

ALTER SEQUENCE address_book_id_seq RESTART WITH 100;
ALTER SEQUENCE recruiter_id_seq RESTART WITH 5;

INSERT INTO RECRUITMENT_REQUEST (id, created_by, created_date, last_modified_by, last_modified_date, status, version, recruiter_id, submission_date)
VALUES
    (1, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 1, current_date());

INSERT INTO recruit (id, created_by, created_date, last_modified_by, last_modified_date, status, version, skills_group, first_name, last_name, dob, nic, address_book_id, approved, recruitment_req_id, recruited_date)
VALUES
    (1, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'MASONRY', 'Bobba', 'Fett',
        PARSEDATETIME('1985-06-15', 'yyyy-MM-dd'), '857777771V', 10, FALSE, 1,
     PARSEDATETIME('2014-06-15', 'yyyy-MM-dd')),
    (2, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'MASONRY', 'Jango', 'Fett',
        PARSEDATETIME('1985-06-15', 'yyyy-MM-dd'), '857772221V', 11, FALSE, 1,
     PARSEDATETIME('2014-06-20', 'yyyy-MM-dd')),
    (3, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'MASONRY', 'Imperial',
        'Mason',
        PARSEDATETIME('1985-06-15', 'yyyy-MM-dd'), '857774441V', 12, FALSE, 1,
     PARSEDATETIME('2014-03-15', 'yyyy-MM-dd')),
    (4, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'MASONRY', 'Rebel',
        'Mason',
        PARSEDATETIME('1985-06-15', 'yyyy-MM-dd'), '857722221V', 13, FALSE, 1,
     PARSEDATETIME('2015-06-15', 'yyyy-MM-dd')),
    (5, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1, 'MASONRY', 'Pirate',
        'Mason',
        PARSEDATETIME('1985-06-15', 'yyyy-MM-dd'), '857732421V', 14, FALSE, 1,
     PARSEDATETIME('2017-06-15', 'yyyy-MM-dd'));

ALTER SEQUENCE recruit_id_seq RESTART WITH 50;

INSERT INTO user (id, created_by, created_date, last_modified_by, last_modified_date, status, version, username, password, first_name, last_name)
VALUES
    (1, 'harshasuranjith@gmail.com', now(), 'harshasuranjith@gmail.com', now(), 'ACTIVE', 1,
        'harshasuranjith@gmail.com',
        '$2a$10$7q5p9Tm/C37hdvprnkX8.uPIg6gzvwKrGxSEsXeU.sJ20xZkKZ356', 'Harsha', 'Amarasiri');

ALTER SEQUENCE user_id_seq RESTART WITH 10;

INSERT INTO role (id, role) VALUES
    (1, 'RECRUITS'),
    (2, 'RECRUITERS'),
    (3, 'COMMISSION');

INSERT INTO user_roles (user_id, role_id) VALUES
    (1, 1), (1, 2), (1, 3);

CREATE VIEW COMMISSION_VIEW AS (
    SELECT
        RR.ID AS ID,
        R.FIRST_NAME,
        R.LAST_NAME,
        R.ID  AS REC_ID,
        RC.MASON_COMMISSION,
        RC.MASON_BONUS,
        RC.CARPENTER_COMMISSION,
        RC.CARPENTER_BONUS,
        RR.SUBMISSION_DATE
    FROM RECRUITER R
        INNER JOIN RECRUITMENT_REQUEST RR ON RR.RECRUITER_ID = R.ID
        INNER JOIN RECRUITMENT_COMMISSION RC ON RR.RECRUITMENT_COMMISSION_ID = RC.ID);
