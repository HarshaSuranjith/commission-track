(function () {
    'use strict';

    init();

    function init() {
        $(document).ready(function () {
            $('.sidenav').sidenav();
            $('#menu').on('click', function () {
                var inst = M.Sidenav.getInstance($('#slide-out')).open();
            })
        });

        $(document).ready(function () {
            $('#recruiters-add-btn').floatingActionButton();
        });

        $(".dropdown-trigger").dropdown();
    }

})();
