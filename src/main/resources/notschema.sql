--DROP ALL OBJECTS;

CREATE TABLE address_book (
    id                 INTEGER      NOT NULL,
    created_by         VARCHAR(255),
    created_date       TIMESTAMP,
    last_modified_by   VARCHAR(255),
    last_modified_date TIMESTAMP,
    status             VARCHAR(255) NOT NULL,
    version            INTEGER      NOT NULL,
    address_line1      VARCHAR(700),
    address_line2      VARCHAR(100),
    alt_email          VARCHAR(150),
    country            VARCHAR(100),
    email              VARCHAR(150),
    fixed_phone        VARCHAR(16),
    mobile_phone       VARCHAR(16),
    dob                DATE,
    first_name         VARCHAR(255) NOT NULL,
    last_name          VARCHAR(100) NOT NULL,
    middle_name        VARCHAR(100),
    province_state     VARCHAR(20),
    PRIMARY KEY (id)
);
CREATE TABLE recruit_group (
    id           INTEGER      NOT NULL,
    group_id     VARCHAR(255) NOT NULL,
    recruiter_id INTEGER      NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE recruit (
    id                 INTEGER      NOT NULL,
    created_by         VARCHAR(255),
    created_date       TIMESTAMP,
    last_modified_by   VARCHAR(255),
    last_modified_date TIMESTAMP,
    status             VARCHAR(255) NOT NULL,
    version            INTEGER      NOT NULL,
    approved           BOOLEAN      NOT NULL,
    nic                VARCHAR(255) NOT NULL,
    dob                DATE,
    first_name         VARCHAR(255) NOT NULL,
    last_name          VARCHAR(100) NOT NULL,
    middle_name        VARCHAR(100),
    address_book_id    INTEGER,
    PRIMARY KEY (id)
);
CREATE TABLE recruit_group (
    group_id     INTEGER NOT NULL,
    recruit_id INTEGER NOT NULL,
    PRIMARY KEY (group_id, recruit_id)
);
CREATE TABLE monthly_commission (
    id                 INTEGER        NOT NULL,
    created_by         VARCHAR(255),
    created_date       TIMESTAMP,
    last_modified_by   VARCHAR(255),
    last_modified_date TIMESTAMP,
    status             VARCHAR(255)   NOT NULL,
    version            INTEGER        NOT NULL,
    bonus              DECIMAL(19, 2) NOT NULL,
    commission         DECIMAL(19, 2) NOT NULL,
    finalized          BOOLEAN,
    payment_status     VARCHAR(255),
    recruiter_id       INTEGER        NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE recruiter (
    id                 INTEGER      NOT NULL,
    created_by         VARCHAR(255),
    created_date       TIMESTAMP,
    last_modified_by   VARCHAR(255),
    last_modified_date TIMESTAMP,
    status             VARCHAR(255) NOT NULL,
    version            INTEGER      NOT NULL,
    address_book_id    INTEGER,
    dob                DATE,
    first_name         VARCHAR(255) NOT NULL,
    last_name          VARCHAR(100) NOT NULL,
    middle_name        VARCHAR(100),
    code               VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE recruiter
    DROP CONSTRAINT IF EXISTS UK_5vjnygh85x6tjal8qidf5jtvs;
ALTER TABLE recruiter
    ADD CONSTRAINT UK_5vjnygh85x6tjal8qidf5jtvs UNIQUE (code);
CREATE SEQUENCE address_book_id_seq START WITH 1 INCREMENT BY 50;
CREATE SEQUENCE recruit_group_id_seq START WITH 1 INCREMENT BY 50;
CREATE SEQUENCE recruit_id_seq START WITH 1 INCREMENT BY 50;
CREATE SEQUENCE monthly_commission_id_seq START WITH 1 INCREMENT BY 50;
CREATE SEQUENCE recruiter_id_seq START WITH 1 INCREMENT BY 50;
ALTER TABLE recruit
    ADD CONSTRAINT fk_recruit_addressbook FOREIGN KEY (address_book_id) REFERENCES address_book;
ALTER TABLE recruit_group
    ADD CONSTRAINT fk_recruit_group FOREIGN KEY (recruit_id) REFERENCES recruit;
ALTER TABLE recruit_group
    ADD CONSTRAINT fk_group_worker_recruit FOREIGN KEY (group_id) REFERENCES recruit_group;
ALTER TABLE monthly_commission
    ADD CONSTRAINT fk_recruiter_monthly_commission FOREIGN KEY (recruiter_id) REFERENCES recruiter;
ALTER TABLE recruiter
    ADD CONSTRAINT fk_recruiter_address_book FOREIGN KEY (address_book_id) REFERENCES address_book;
