package lk.tech99x.assessment.commisiontracker.web;

import lk.tech99x.assessment.commisiontracker.domain.commission.CommissionDTO;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.*;
import lk.tech99x.assessment.commisiontracker.service.business.RecruitersManagementService;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.RecruiterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("recruiters")
public class RecruitersController {

    private RecruitersManagementService recruitersManagementService;
    private RecruiterRepository recruiterRepository;

    @Autowired
    public RecruitersController(RecruitersManagementService recruitersManagementService,
                                RecruiterRepository recruiterRepository) {
        this.recruitersManagementService = recruitersManagementService;
        this.recruiterRepository = recruiterRepository;
    }

    @ModelAttribute("skillsGroups")
    private List<SkillsGroup> getAllSkillsGroups() {
        return Arrays.asList(SkillsGroup.values());
    }


    @GetMapping("/all")
    public String getRecruiters(Model model) {

        Set<Recruiter> recruiters = this.recruitersManagementService.findAll();
        model.addAttribute("recruiters", recruiters);
        return "recruiters/all";

    }

    @GetMapping("/new")
    public String getRecruiterAddForm(Model model) {

        Recruiter recruiter =
            Recruiter.builder()
                .addressBook(new AddressBook())
                .personal(new Personal())
                .build();
        model.addAttribute("recruiter", recruiter);
        return "recruiters/recruiter";
    }

    @PostMapping("/new")
    public String addRecruiter(@ModelAttribute Recruiter recruiter, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "recruiters/recruiter";
        }
        recruiter = this.recruitersManagementService.addRecruiter(recruiter);
        return "recruiters/recruiter";
    }


    @GetMapping("/{recruiterCode}/commission")
    public String getRecruiterCommission(@PathVariable(name = "recruiterCode") String recruiterCode, Model model) {
        return "recruiters/commission";
    }

    @GetMapping(value = "/{recruiterCode}/recruitment-record")
    public String getRecruitmentRecord(@RequestParam(required = false) Month month, @PathVariable String recruiterCode, Model model) {

        if (month == null) {
            month = LocalDate.now().getMonth();
        }
        RecruitmentRecordDTO recruitmentRecord = this.recruitersManagementService.getRecruitmentRecordForMonth(recruiterCode, month);
        model.addAttribute("record", recruitmentRecord);
        return "recruiters/recruitment-record";
    }

    @GetMapping("/{recruiterCode}/recruitment-request")
    public String getRecruitmentRequestForm(@PathVariable String recruiterCode, Model model) {

        RecruitmentRequest recruitmentRequest = new RecruitmentRequest();
        recruitmentRequest.getRecruits().add(new Recruit());

        model.addAttribute("recruitmentRequest", recruitmentRequest);
        return "recruiters/recruitment-request";
    }

    @RequestMapping(value = "/{recruiterCode}/recruitment-request", method = RequestMethod.POST)
    public String saveRecruitmentRequest(@PathVariable @ModelAttribute String recruiterCode,
                                         @ModelAttribute @Valid RecruitmentRequest recruitmentRequest,
                                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "recruiters/recruitment-request";
        }
        if (recruitmentRequest.getRecruiterId() == null) {
            Integer recruiterId = this.recruitersManagementService.getIdByRecruiterCode(recruiterCode);
            recruitmentRequest.setRecruiterId(recruiterId);
        }
        recruitmentRequest = this.recruitersManagementService.addRecruitmentRequest(recruitmentRequest);
        return "recruiters/recruitment-request";
    }

    @RequestMapping(value = "/{recruiterCode}/recruitment-request", params = {"addRow"})
    public String addRow(@PathVariable @ModelAttribute String recruiterCode,
                         @ModelAttribute RecruitmentRequest recruitmentRequest) {
        recruitmentRequest.getRecruits().add(new Recruit());
        return "recruiters/recruitment-request";
    }

    @RequestMapping(value = "/{recruiterCode}/recruitment-request", params = {"remove"})
    public String removeRow(@PathVariable @ModelAttribute String recruiterCode,
                            @ModelAttribute RecruitmentRequest recruitmentRequest) {
        recruitmentRequest.getRecruits().add(new Recruit());
        return "recruiters/recruitment-request";
    }


    @GetMapping("/{recruiterCode}/monthly-commission")
    public String getMonthlyCommission(@PathVariable String recruiterCode,
                                       @RequestParam(name = "month", required = false) String month,
                                       Model model) {

        Month m = (month != null) ? Month.valueOf(month) : LocalDate.now().getMonth();

        CommissionDTO commissionDTO = this.recruitersManagementService.getCommissionDetailsForMonth(recruiterCode, m);
        model.addAttribute("commissionDTO", commissionDTO);
        return "recruiters/monthly-commission";
    }
}
