package lk.tech99x.assessment.commisiontracker.web;

import lk.tech99x.assessment.commisiontracker.domain.security.User;
import lk.tech99x.assessment.commisiontracker.service.security.AccessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AccessController {

    private AccessManagementService accessManagementService;

    @Autowired
    public AccessController(AccessManagementService accessManagementService) {
        this.accessManagementService = accessManagementService;
    }

    @GetMapping("/")
    public String getIndex() {
        return "redirect:/signin";
    }


    @GetMapping(value = {"/signin"})
    public String getLogin() {
        return "global/signin";
    }


    @GetMapping("/signup")
    public ModelAndView getRegistration(ModelMap model) {
        model.addAttribute("user", new User());
        return new ModelAndView("global/signup", model);
    }


    @PostMapping("/signup")
    public String getRegistration(@ModelAttribute @Valid User user, BindingResult bindingResult) {

        if (!bindingResult.hasErrors()) {
            user = this.accessManagementService.saveUser(user);
            return "redirect:global/signin";
        }
        if (this.accessManagementService.userExistsByUsername(user)) {
            bindingResult.rejectValue("username", "Username already exists");
        }
        return "global/signup";
    }
}
