package lk.tech99x.assessment.commisiontracker.domain.embeddable;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@Getter
@Setter
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Personal {

    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name = "first_name", length = 255, nullable = false)
    private String firstName;

    @Size(max = 100)
    @Column(name = "middle_name", length = 100)
    private String middleName;

    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name = "last_name", length = 100, nullable = false)
    private String lastName;

    @Column(name = "dob")
    private LocalDate dateOfBirth;


}
