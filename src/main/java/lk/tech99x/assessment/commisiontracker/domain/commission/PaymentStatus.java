package lk.tech99x.assessment.commisiontracker.domain.commission;

public enum PaymentStatus {

    PAID,
    PENDING,
    SUSPENDED

}
