package lk.tech99x.assessment.commisiontracker.domain;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitGroups;
import lk.tech99x.assessment.commisiontracker.service.business.Impl.RecruitersManagementServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Serves as the base class for all entities in general
 * All common entity attributes are defined here and subclassed from this;
 */


@MappedSuperclass
@Getter
@Setter
public abstract class AbstractAuditable implements Serializable{

    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false)
    private LocalDateTime createdDate;

    @LastModifiedBy
    @Column(name = "last_modified_by", nullable = false)
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date", nullable = false)
    private LocalDateTime lastModifiedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status = Status.ACTIVE;

    @Version
    @Column(name = "version", nullable = false)
    private Integer version;



}
