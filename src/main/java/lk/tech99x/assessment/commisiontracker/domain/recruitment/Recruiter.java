package lk.tech99x.assessment.commisiontracker.domain.recruitment;

import lk.tech99x.assessment.commisiontracker.domain.AbstractAuditable;
import lk.tech99x.assessment.commisiontracker.domain.commission.MonthlyCommission;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SQLDelete(sql = "UPDATE recruiter SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
@EntityListeners(AuditingEntityListener.class)
@Table(name = "recruiter", uniqueConstraints = {
    @UniqueConstraint(name = "uc_recruiter_code", columnNames = {"code"})
})
public class Recruiter extends AbstractAuditable {

    @Id
    @SequenceGenerator(name = "recruiter_id_seq_gen", sequenceName = "recruiter_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "recruiter_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Integer id;

    @Size(min = 5, max = 20, message = "recruiter code length has to be between 5 characters and 20 characters")
    @Column(name = "code", nullable = false, updatable = false, unique = true, length = 20)
    private String recruiterCode;

    @Valid
    @Embedded
    private Personal personal;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9\\s]+", message = "must be letter and number characters only")
    @Size(min = 6, max = 30)
    @Column(name = "nic", length = 30, nullable = false, updatable = false, unique = true)
    private String nic;

    @Column(name = "photo_url", length = 500)
    private String photoUrl;

    @Column(name = "address_book_id", insertable = false, updatable = false)
    private Integer addressBookId;

    @Valid
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_book_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_recruiter_address_book"))
    private AddressBook addressBook;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "recruiter_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_recruiter_recruitment_requests"))
    private Set<RecruitmentRequest> recruitmentRequests;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "recruiter_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_recruiter_monthly_commission")
    )
    private Set<MonthlyCommission> monthlyCommissions;

    @Transient
    public boolean isValid() {
        return true;
    }
}
