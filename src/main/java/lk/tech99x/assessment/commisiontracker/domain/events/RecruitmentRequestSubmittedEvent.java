package lk.tech99x.assessment.commisiontracker.domain.events;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRequest;
import lombok.Getter;

@Getter
public class RecruitmentRequestSubmittedEvent {

    public final RecruitmentRequest recruitmentRequest;

    public RecruitmentRequestSubmittedEvent(RecruitmentRequest recruitmentRequest) {
        this.recruitmentRequest = recruitmentRequest;
    }
}
