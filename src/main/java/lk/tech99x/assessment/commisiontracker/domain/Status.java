package lk.tech99x.assessment.commisiontracker.domain;

public enum Status {

    ACTIVE,
    INACTIVE,
    DELETED

}
