package lk.tech99x.assessment.commisiontracker.domain.recruitment;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SQLDelete(sql = "UPDATE recruiter SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
@EntityListeners(AuditingEntityListener.class)
@Table(name = "recruit_group")
public class RecruitGroups {

    @Id
    @SequenceGenerator(name = "recruit_group_id_seq_gen", sequenceName = "recruit_group_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "recruit_group_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer id;

    @Column(name = "skills_group", nullable = false, updatable = false)
    private SkillsGroup skillsGroup;

    @Column(name = "group_id", nullable = false, updatable = false)
    private String groupId;

    @Column(name = "recruiter_id")
    private Integer recruiterId;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "group_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_group_worker_recruit"))
    private Set<Recruit> recruits;

}
