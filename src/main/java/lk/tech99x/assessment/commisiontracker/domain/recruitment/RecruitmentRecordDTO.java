package lk.tech99x.assessment.commisiontracker.domain.recruitment;

import lk.tech99x.assessment.commisiontracker.domain.commission.MonthlyCommission;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter @Setter @Builder
public class RecruitmentRecordDTO {

    private Recruiter recruiter;

    private Set<Recruit> recruits;

    private MonthlyCommission monthlyCommission;

}
