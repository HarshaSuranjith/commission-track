package lk.tech99x.assessment.commisiontracker.domain.recruitment;

import lk.tech99x.assessment.commisiontracker.domain.AbstractAuditable;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;


@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "address_book")
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE address_book SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
public class AddressBook extends AbstractAuditable {

    @Id
    @SequenceGenerator(name = "address_book_id_seq_gen", sequenceName = "address_book_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "address_book_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer id;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "firstName", column = @Column(name = "first_name", nullable = true)),
        @AttributeOverride(name = "lastName", column = @Column(name = "last_name", nullable = true))
    })
    private Personal personal;

    @Size(max = 16)
    @Column(name = "mobile_phone", length = 16)
    private String mobilePhone;

    @Size(max = 16)
    @Column(name = "fixed_phone", length = 16)
    private String fixedPhone;

    @Email
    @Column(name = "email", length = 150)
    private String email;

    @Email
    @Column(name = "alt_email", length = 150)
    private String altEmail;

    @Size(max = 700)
    @Column(name = "address_line1", length = 700)
    private String addressLine1;

    @Size(max = 100)
    @Column(name = "address_line2", length = 100)
    private String addressLine2;

    @Size(max = 20)
    @Column(name = "province_state", length = 20)
    private String provinceOrState;

    @Size(max = 100)
    @Column(name = "country", length = 100)
    private String country;


    // Builder

    public static Builder builder() {
        return new Builder();
    }

    AddressBook(Builder builder) {
        this.id = builder.id;
        this.mobilePhone = builder.mobilePhone;
        this.fixedPhone = builder.fixedPhone;
        this.email = builder.email;
        this.altEmail = builder.altEmail;
        this.addressLine1 = builder.addressLine1;
        this.addressLine2 = builder.addressLine2;
        this.provinceOrState = builder.provinceOrState;
        this.country = builder.country;
    }

    public static class Builder {

        private Integer id;
        private String mobilePhone;
        private String fixedPhone;
        private String email;
        private String altEmail;
        private String addressLine1;
        private String addressLine2;
        private String provinceOrState;
        private String country;


        public Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder mobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
            return this;
        }

        public Builder fixedPhone(String fixedPhone) {
            this.fixedPhone = fixedPhone;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder altEmail(String altEmail) {
            this.altEmail = altEmail;
            return this;
        }

        public Builder addressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
            return this;
        }

        public Builder addressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
            return this;
        }

        public Builder provinceOrState(String provinceOrState) {
            this.provinceOrState = provinceOrState;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public AddressBook build() {
            return new AddressBook(this);
        }

    }

}
