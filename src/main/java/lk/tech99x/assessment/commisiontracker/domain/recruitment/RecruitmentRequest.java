package lk.tech99x.assessment.commisiontracker.domain.recruitment;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lk.tech99x.assessment.commisiontracker.domain.AbstractAuditable;
import lk.tech99x.assessment.commisiontracker.domain.Constants;
import lk.tech99x.assessment.commisiontracker.domain.commission.RecruitmentCommission;
import lk.tech99x.assessment.commisiontracker.util.Partitioner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE recruitment_request SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
@Table(name = "recruitment_request")
public class RecruitmentRequest extends AbstractAuditable {


    @Id
    @SequenceGenerator(name = "recruitment_request_seq_gen", sequenceName = "recruitment_request_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "recruitment_request_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Integer id;

    @Valid
    @NotEmpty(message = "Provided request has no recruits")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "recruitment_request_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_recruitment_request_recruit"))
    private List<Recruit> recruits = new ArrayList<>();

    @Column(name = "submission_date", nullable = false, updatable = false)
    private LocalDateTime submissionDate;

    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "recruitment_commission_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_recruitment_request_commission"))
    private RecruitmentCommission recruitmentCommission;

    @Column(name = "recruiter_id", nullable = false)
    private Integer recruiterId;

    @Transient
    public Set<Recruit> getAllMasons() {
        return this.recruits.stream().filter(c -> c.getSkillsGroup().equals(SkillsGroup.MASONRY)).collect(Collectors.toSet());
    }

    @Transient
    public Set<Recruit> getAllCarpenters() {
        return this.recruits.stream().filter(c -> c.getSkillsGroup().equals(SkillsGroup.CARPENTRY)).collect(Collectors.toSet());
    }

    @Transient
    public boolean isMasonGroupsAvailable() {
        return this.getAllMasons().size() >= Constants.GROUP_SIZE;
    }

    @Transient
    public boolean isCarpenterGroupsAvailable() {
        return this.getAllCarpenters().size() >= Constants.GROUP_SIZE;
    }

    @Transient
    public Integer getMasonGroupCount() {
        return this.getAllMasons().size() / Constants.GROUP_SIZE;
    }

    @Transient
    public Integer getCarpenterGroupCount() {
        return this.getAllCarpenters().size() / Constants.GROUP_SIZE;
    }

    @Transient
    public boolean isValid() {
        return !this.recruits.isEmpty() &&
            (recruiterId != null || (recruiterCode != null && recruiterCode.isEmpty()));
    }

    @Transient
    public String recruiterCode;

    public Set<RecruitGroups> partition(Partitioner partitioner) {
        return partitioner.partitionGroups(this.recruits);
    }
}
