package lk.tech99x.assessment.commisiontracker.domain.security;


import lk.tech99x.assessment.commisiontracker.domain.AbstractAuditable;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user", uniqueConstraints = {
    @UniqueConstraint(name = "uc_username", columnNames = {"username"})
})
public class User extends AbstractAuditable implements UserDetails {

    @Id
    @SequenceGenerator(name = "user_id_seq_gen", sequenceName = "user_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "user_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer id;

    @NotNull
    @Size(max = 40)
    @Column(name = "username", nullable = false, updatable = false)
    private String username;

    @NotNull
    @Column(name = "password", nullable = false, updatable = false)
    private String password;

    @Valid
    @Embedded
    private Personal personal = new Personal();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
        joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), foreignKey = @ForeignKey(name = "fk_user_roles"),
        inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), inverseForeignKey = @ForeignKey(name = "fk_role_users"))
    private Set<Role> assignedAuthorities = new HashSet<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Assert.notEmpty(assignedAuthorities, "Authorities cannot be empty");
        return assignedAuthorities.stream().map(
            r -> new SimpleGrantedAuthority(r.getRole().toUpperCase())
        ).collect(Collectors.toSet());
    }

    /**
     * @implNote : All following flags returns true considering the nature of application
     * If needed more concrete and specific logic can be exposed via entity attributes mapped to the database
     */

    @Override
    public boolean isAccountNonExpired() {

        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public boolean isValid() {

        return personal.getFirstName() != null && !personal.getFirstName().isEmpty() &&
            personal.getLastName() != null && !personal.getLastName().isEmpty() &&
            !username.isEmpty() &&
            !password.isEmpty();

    }
}
