package lk.tech99x.assessment.commisiontracker.domain;

public class Constants {

    public static final int GROUP_SIZE = 5;
    public static final String DEFAULT_CURRENCY_UNIT = "USD";

}
