package lk.tech99x.assessment.commisiontracker.domain.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "role", uniqueConstraints = {
    @UniqueConstraint(name = "uc_role", columnNames = {"role"})
})
public class Role {

    @Id
    @SequenceGenerator(name = "role_id_seq_gen", sequenceName = "role_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "role_id_seq_gen", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "role", unique = true, nullable = false)
    private String role;

}
