package lk.tech99x.assessment.commisiontracker.domain.commission;

import lk.tech99x.assessment.commisiontracker.domain.AbstractAuditable;
import lk.tech99x.assessment.commisiontracker.domain.Constants;
import lk.tech99x.assessment.commisiontracker.service.factory.AbstractObjectFactory;
import lk.tech99x.assessment.commisiontracker.util.MonetaryAmountConverter;
import lk.tech99x.assessment.commisiontracker.util.YearMonthConverter;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.persistence.*;
import java.time.YearMonth;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SQLDelete(sql = "UPDATE monthly_commission SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
@EntityListeners(AuditingEntityListener.class)
@Table(name = "monthly_commission", uniqueConstraints = {
    @UniqueConstraint(name = "uk_recruiter_monthly_commission", columnNames = {"month", "recruiter_id"})
})
public class MonthlyCommission extends AbstractAuditable {

    @Id
    @SequenceGenerator(name = "monthly_commission_id_seq_gen", sequenceName = "monthly_commission_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "monthly_commission_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer id;

    @Convert(converter = YearMonthConverter.class)
    @Column(name = "month", nullable = false, length = 10)
    private YearMonth month = YearMonth.now();

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "commission", nullable = false)
    private MonetaryAmount commission = AbstractObjectFactory.getDefaultMonetaryAmount();

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "bonus", nullable = false)
    private MonetaryAmount bonus = AbstractObjectFactory.getDefaultMonetaryAmount();

    @Column(name = "recruiter_id", nullable = false, updatable = false)
    private Integer recruiterId;

    @Column(name = "finalized", nullable = false)
    private Boolean isFinalized;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Transient
    private MonetaryAmount getTotal() {
        return bonus.add(commission);
    }

    public void add(MonetaryAmount commission, MonetaryAmount bonus) {
        this.commission = this.commission.add(commission);
        this.bonus = this.bonus.add(bonus);
    }
}
