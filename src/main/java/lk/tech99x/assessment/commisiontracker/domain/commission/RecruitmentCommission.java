package lk.tech99x.assessment.commisiontracker.domain.commission;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRequest;
import lk.tech99x.assessment.commisiontracker.util.CommissionCalc;
import lk.tech99x.assessment.commisiontracker.util.MonetaryAmountConverter;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import java.beans.Transient;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE recruitment_commission SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
@EntityListeners(AuditingEntityListener.class)
@Table(name = "recruitment_commission")
public class RecruitmentCommission {

    @Id
    @SequenceGenerator(name = "recruitment_commission_id_seq_gen", sequenceName = "recruitment_commission_id_seq")
    @GeneratedValue(generator = "recruitment_commission_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer id;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "mason_commission", nullable = false)
    private MonetaryAmount masonCommission;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "mason_bonus", nullable = false)
    private MonetaryAmount masonBonus;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "carpenter_commission", nullable = false)
    private MonetaryAmount carpenterCommission;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "carpenter_bonus", nullable = false)
    private MonetaryAmount carpenterBonus;

    @OneToOne(mappedBy = "recruitmentCommission")
    private RecruitmentRequest recruitmentRequest;

    @Transient
    public static RecruitmentCommission getCommissionFor(RecruitmentRequest request
    ) {
        return RecruitmentCommission.builder()
            .masonCommission(
                CommissionCalc.getMasonCommission(request.getAllMasons().size()))
            .carpenterCommission(
                CommissionCalc.getCarpenterCommission(request.getAllCarpenters().size()))
            .masonBonus(
                CommissionCalc.getMasonBonus(request.getMasonGroupCount()))
            .carpenterBonus(
                CommissionCalc.getCarpenterBonus(request.getCarpenterGroupCount()))
            .build();
    }

}
