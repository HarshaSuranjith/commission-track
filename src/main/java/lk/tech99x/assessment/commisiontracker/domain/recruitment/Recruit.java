package lk.tech99x.assessment.commisiontracker.domain.recruitment;

import lk.tech99x.assessment.commisiontracker.domain.AbstractAuditable;
import lk.tech99x.assessment.commisiontracker.domain.embeddable.Personal;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "recruit")
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE address_book SET status = DELETED WHERE id = ?")
@Where(clause = "status = 'ACTIVE'")
public class Recruit extends AbstractAuditable {

    @Id
    @SequenceGenerator(name = "recruit_id_seq_gen", sequenceName = "recruit_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "recruit_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer Id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "skills_group", nullable = false)
    private SkillsGroup skillsGroup;

    @Valid
    @Embedded
    private Personal personal;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9\\s]+", message = "must be letter and number characters only")
    @Size(min = 6, max = 30)
    @Column(name = "nic", nullable = false, length = 30)
    private String nic;

    @Column(name = "approved", nullable = false)
    private boolean approved;

    @Column(name = "recruited_date", nullable = false)
    private LocalDateTime recruitedDate = LocalDateTime.now();

    @Column(name = "address_book", insertable = false, updatable = false)
    private Integer addressBookId;

    @Column(name = "recruitment_req_id")
    private Integer recruitmentRequestId;

    @Valid
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_book_id", referencedColumnName = "id",
        foreignKey = @ForeignKey(name = "fk_recruit_addressbook")
    )
    private AddressBook addressBook;

}

