package lk.tech99x.assessment.commisiontracker.domain.commission;

import lk.tech99x.assessment.commisiontracker.util.MonetaryAmountConverter;
import lombok.Getter;
import lombok.Setter;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "commission_view")
public class CommissionViewDTO {

    // recruitment_request_id
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    //recruiter_id
    @Column(name = "rec_id")
    private Integer recruiterId;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "mason_commission")
    private MonetaryAmount masonCommission;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "mason_bonus")
    private MonetaryAmount masonBonus;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "carpenter_commission")
    private MonetaryAmount carpenterCommission;

    @Convert(converter = MonetaryAmountConverter.class)
    @Column(name = "carpenter_bonus")
    private MonetaryAmount carpenterBonus;

    @Column(name = "submission_date")
    private LocalDateTime submissionDate;
}
