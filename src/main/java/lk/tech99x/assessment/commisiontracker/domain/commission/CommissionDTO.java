package lk.tech99x.assessment.commisiontracker.domain.commission;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruiter;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
public class CommissionDTO {

    private Recruiter recruiter;

    private Set<RecruitmentRequest> recruitmentRequests;

    private MonthlyCommission monthlyCommission;


}
