package lk.tech99x.assessment.commisiontracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SpringBootWebSecurityConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(dateTimeProviderRef = "temporalAuditProvider", auditorAwareRef = "userAuditProvider")
public class CommissionTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommissionTrackerApplication.class, args);
    }

}
