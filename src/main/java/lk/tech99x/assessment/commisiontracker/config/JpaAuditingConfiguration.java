package lk.tech99x.assessment.commisiontracker.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;

@Configuration
public class JpaAuditingConfiguration {


    @Bean
    AuditorAware<String> userAuditProvider() {
        return new UsernameAuditorAware();
    }

    @Bean
    DateTimeProvider temporalAuditProvider() {
        return new temporalAuditProvider();
    }

    private class temporalAuditProvider implements DateTimeProvider {
        @Override
        public Optional<TemporalAccessor> getNow() {
            return Optional.of(LocalDateTime.now());
        }
    }


    private class UsernameAuditorAware implements AuditorAware<String> {
        @Override
        public Optional<String> getCurrentAuditor() {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            if (authentication == null) {
                return Optional.of("unauthenticated");
            }

            if (!authentication.isAuthenticated() || authentication.getName().equals("anonymousUser")) {
                return Optional.of(((WebAuthenticationDetails) authentication.getDetails()).getRemoteAddress());
            }

            return Optional.of(authentication.getName());
        }
    }
}
