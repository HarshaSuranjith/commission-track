package lk.tech99x.assessment.commisiontracker.service.security.Impl;

import lk.tech99x.assessment.commisiontracker.domain.security.User;
import lk.tech99x.assessment.commisiontracker.service.business.exceptions.CommissionTrackException;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.RoleRepository;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.UserRepository;
import lk.tech99x.assessment.commisiontracker.service.security.AccessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccessManagementServiceImpl implements AccessManagementService {

    private BCryptPasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Value("${security.auth.grant-all}")
    Boolean grantAll;


    @Autowired
    public AccessManagementServiceImpl(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository, RoleRepository roleRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public User saveUser(User user) {
        if (!user.isValid()) {
            throw new CommissionTrackException("User details are invalid");
        }
        String encPasswd = this.passwordEncoder.encode(user.getPassword());
        user.setPassword(encPasswd);


        /**
         *  No permission management implemented therefore add all permissions
         */
        if (grantAll) {
            user.getAssignedAuthorities().addAll(this.roleRepository.findAll());
        }


        user = this.userRepository.save(user);
        user.setPassword(null);
        return user;
    }

    @Override
    public boolean userExistsByUsername(User user) {
        return this.userRepository.findByUsername(user.getUsername()) != null;
    }
}
