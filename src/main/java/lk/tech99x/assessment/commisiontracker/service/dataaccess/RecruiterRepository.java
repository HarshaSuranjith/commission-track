package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruit;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruiter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RecruiterRepository extends JpaRepository<Recruiter, Integer> {

    Optional<Recruiter> findByRecruiterCode(String recruiterCode);

    @Query(
        "SELECT Recruit FROM Recruiter recr " +
            "JOIN  recr.recruitmentRequests RecruitmentRequest " +
            "JOIN RecruitmentRequest.recruits Recruit " +
            "WHERE recr.recruiterCode = :code AND RecruitmentRequest.submissionDate BETWEEN :fromDate AND :toDate"
    )
    Set<Recruit> findAllRecruitsForMonthAndRecruiter(@Param("fromDate") LocalDateTime from, @Param("toDate") LocalDateTime to, @Param("code") String recruiterCode);

    @Query(
        "SELECT CASE WHEN (COUNT(rec) > 0)  THEN true ELSE false END FROM Recruiter rec WHERE rec.nic= :nic"
    )
    boolean existsByNic(@Param("nic") String nic);

    Recruiter getByRecruiterCode(String recruiterCode);
}
