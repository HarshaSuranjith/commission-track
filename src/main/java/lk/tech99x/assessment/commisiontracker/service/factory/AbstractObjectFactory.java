package lk.tech99x.assessment.commisiontracker.service.factory;

import lk.tech99x.assessment.commisiontracker.domain.Constants;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

public class AbstractObjectFactory {

    public static MonetaryAmount getDefaultMonetaryAmount() {
        return Monetary.getDefaultAmountFactory().setNumber(0).setCurrency(Constants.DEFAULT_CURRENCY_UNIT).create();
    }

}
