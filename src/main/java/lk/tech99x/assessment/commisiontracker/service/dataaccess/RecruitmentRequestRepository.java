package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Set;

@Repository
public interface RecruitmentRequestRepository extends JpaRepository<RecruitmentRequest, Integer> {

    Set<RecruitmentRequest> findBySubmissionDateBetween(LocalDateTime from, LocalDateTime to);

}
