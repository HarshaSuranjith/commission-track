package lk.tech99x.assessment.commisiontracker.service;

import lk.tech99x.assessment.commisiontracker.domain.events.RecruitmentRequestSubmittedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class GlobalApplicationEventHandler {

    @Autowired
    public GlobalApplicationEventHandler() {
    }

    @Async
    @EventListener
    public void handleAppointmentConfirmedEvent(RecruitmentRequestSubmittedEvent event) {

        /*
            Handle Recruitment Submit Event specific operations here
         */

    }

}
