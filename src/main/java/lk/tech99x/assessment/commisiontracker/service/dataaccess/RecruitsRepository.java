package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RecruitsRepository extends JpaRepository<Recruit, Integer>{
    Set<Recruit> findByRecruitmentRequestId(Integer recruiterId);
}
