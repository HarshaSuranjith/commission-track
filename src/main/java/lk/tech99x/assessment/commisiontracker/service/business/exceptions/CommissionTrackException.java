package lk.tech99x.assessment.commisiontracker.service.business.exceptions;

public class CommissionTrackException extends RuntimeException {
    public CommissionTrackException(String message, Exception e) {
        super(message, e);
    }

    public CommissionTrackException(String message) {
        super(message);
    }
}
