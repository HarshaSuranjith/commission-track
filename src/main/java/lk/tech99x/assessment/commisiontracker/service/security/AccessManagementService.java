package lk.tech99x.assessment.commisiontracker.service.security;

import lk.tech99x.assessment.commisiontracker.domain.security.User;
import org.springframework.stereotype.Service;

@Service
public interface AccessManagementService {
    User saveUser(User user);

    boolean userExistsByUsername(User user);
}
