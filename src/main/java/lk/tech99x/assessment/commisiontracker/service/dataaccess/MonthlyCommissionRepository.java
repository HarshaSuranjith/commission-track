package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.commission.MonthlyCommission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.YearMonth;
import java.util.Optional;

@Repository
public interface MonthlyCommissionRepository extends JpaRepository<MonthlyCommission, Integer> {
    Optional<MonthlyCommission> findByMonthAndRecruiterId(YearMonth now, Integer recruiterId);

    MonthlyCommission getByMonthAndRecruiterId(YearMonth now, Integer recruiterId);
}
