package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.commission.CommissionViewDTO;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Set;

@Repository
public interface CommissionViewRepository extends BaseViewRepository<CommissionViewDTO, Integer> {

    Set<CommissionViewDTO> findAllBySubmissionDateBetween(LocalDateTime from, LocalDateTime to);

}
