package lk.tech99x.assessment.commisiontracker.service.business.Impl;

import lk.tech99x.assessment.commisiontracker.domain.commission.CommissionDTO;
import lk.tech99x.assessment.commisiontracker.domain.commission.MonthlyCommission;
import lk.tech99x.assessment.commisiontracker.domain.commission.RecruitmentCommission;
import lk.tech99x.assessment.commisiontracker.domain.events.RecruitmentRequestSubmittedEvent;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.*;
import lk.tech99x.assessment.commisiontracker.service.business.RecruitersManagementService;
import lk.tech99x.assessment.commisiontracker.service.business.exceptions.CommissionTrackException;
import lk.tech99x.assessment.commisiontracker.service.business.exceptions.RecruitersNotFoundException;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.MonthlyCommissionRepository;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.RecruitGroupsRepository;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.RecruiterRepository;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.RecruitmentRequestRepository;
import lk.tech99x.assessment.commisiontracker.service.factory.AbstractObjectFactory;
import lk.tech99x.assessment.commisiontracker.util.DurationUtil;
import lk.tech99x.assessment.commisiontracker.util.Partitioner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.YearMonth;
import java.util.*;

@Service
public class RecruitersManagementServiceImpl implements RecruitersManagementService {

    private final RecruiterRepository recruiterRepository;
    private final RecruitmentRequestRepository recruitmentRequestRepository;
    private final RecruitGroupsRepository recruitGroupRepository;
    private final ApplicationEventPublisher publisher;
    private final Partitioner partitioner;
    private final MonthlyCommissionRepository monthlyCommissionRepository;

    @Autowired
    public RecruitersManagementServiceImpl(RecruiterRepository recruiterRepository, RecruitmentRequestRepository recruitmentRequestRepository, RecruitGroupsRepository recruitGroupRepository, ApplicationEventPublisher publisher, Partitioner partitioner, MonthlyCommissionRepository monthlyCommissionRepository) {
        this.recruiterRepository = recruiterRepository;
        this.recruitmentRequestRepository = recruitmentRequestRepository;
        this.recruitGroupRepository = recruitGroupRepository;
        this.publisher = publisher;
        this.partitioner = partitioner;
        this.monthlyCommissionRepository = monthlyCommissionRepository;
    }

    @Override
    public Set<Recruiter> findAll() {

        List<Recruiter> recruiters = this.recruiterRepository.findAll();
        if (recruiters.isEmpty()) {
            throw new RecruitersNotFoundException();
        }
        return new HashSet<>(recruiters);
    }

    @Override
    public Recruiter findByRecruiterCode(String recruiterCode) {
        Assert.notNull(recruiterCode, "Recruiter Code provided is null");
        Optional<Recruiter> recruiter = this.recruiterRepository.findByRecruiterCode(recruiterCode);
        if (!recruiter.isPresent()) {
            throw new RecruitersNotFoundException();
        }
        return recruiter.get();
    }

    @Override
    public RecruitmentRecordDTO getRecruitmentRecordForMonth(String recruiterCode, Month month) {
        Optional<Recruiter> recruiter = this.recruiterRepository.findByRecruiterCode(recruiterCode);
        if (!recruiter.isPresent()) {
            throw new CommissionTrackException("Recruiter not found");
        }


        LocalDateTime startOfMonth = DurationUtil.getLastMonthDurations().get(DurationUtil.START_OF_MONTH);
        LocalDateTime endOfMonth = DurationUtil.getLastMonthDurations().get(DurationUtil.END_OF_MONTH);


        Set<Recruit> recruits = this.recruiterRepository.findAllRecruitsForMonthAndRecruiter(startOfMonth, endOfMonth, recruiterCode);
        return RecruitmentRecordDTO.builder().recruiter(recruiter.get()).recruits(recruits).build();

    }

    @Override
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = RuntimeException.class)
    public RecruitmentRequest addRecruitmentRequest(RecruitmentRequest recruitmentRequest) {
        try {

            if (!recruitmentRequest.isValid()) {
                throw new CommissionTrackException("Invalid data for recruitment request: " + recruitmentRequest.toString());
            }

            recruitmentRequest.setRecruitmentCommission(
                RecruitmentCommission.getCommissionFor(recruitmentRequest)
            );
            recruitmentRequest.setSubmissionDate(LocalDateTime.now());

            recruitmentRequest = this.recruitmentRequestRepository.saveAndFlush(recruitmentRequest);

            Set<RecruitGroups> groups = recruitmentRequest.partition(partitioner);
            this.recruitGroupRepository.saveAll(groups);

            publisher.publishEvent(new RecruitmentRequestSubmittedEvent(recruitmentRequest));

            return recruitmentRequest;
        } catch (Exception e) {
            throw new CommissionTrackException("Error saving recruitment request", e);
        }
    }

    @Override
    public Recruiter addRecruiter(Recruiter recruiter) {
        try {
            if (!recruiter.isValid()) {
                throw new CommissionTrackException("Invalid data for recruiter: " + recruiter.toString());
            }

            if (this.recruiterRepository.existsByNic(recruiter.getNic())) {
                throw new CommissionTrackException("Recruiter already exists");
            }
            recruiter.setRecruiterCode(this.generateRecruiterCode(recruiter));
            recruiter = this.recruiterRepository.save(recruiter);
            return recruiter;
        } catch (Exception e) {
            throw new CommissionTrackException("Error saving recruiter", e);
        }
    }

    @Override
    public Integer getIdByRecruiterCode(String recruiterCode) {
        try {
            return this.recruiterRepository.findByRecruiterCode(recruiterCode).get().getId();
        } catch (Exception e) {
            throw new CommissionTrackException("Error finding recruiter by code: " + recruiterCode);
        }
    }

    @Override
    public CommissionDTO getCommissionDetailsForMonth(String recruiterCode, Month month) {


        try {
            Recruiter recruiter = this.recruiterRepository.getByRecruiterCode(recruiterCode);

            LocalDateTime startOfMonth = DurationUtil.getLastMonthDurations().get(DurationUtil.START_OF_MONTH);
            LocalDateTime endOfMonth = DurationUtil.getLastMonthDurations().get(DurationUtil.START_OF_MONTH);

            Set<RecruitmentRequest> recruitmentRequests = this.recruitmentRequestRepository.findBySubmissionDateBetween(startOfMonth, endOfMonth);

            MonthlyCommission monthlyCommission = this.monthlyCommissionRepository.getByMonthAndRecruiterId(
                YearMonth.of(startOfMonth.getYear(), startOfMonth.getMonth()), recruiter.getId());

            if (monthlyCommission == null) {
                monthlyCommission = MonthlyCommission.builder()
                    .commission(AbstractObjectFactory.getDefaultMonetaryAmount())
                    .bonus(AbstractObjectFactory.getDefaultMonetaryAmount()).build();
            }

            return CommissionDTO.builder().monthlyCommission(monthlyCommission).recruiter(recruiter).recruitmentRequests(recruitmentRequests).build();
        } catch (Exception e) {
            throw new CommissionTrackException("Unable tp generate CommissionDTO ", e);
        }
    }


    private String generateRecruiterCode(Recruiter recruiter) {
        Assert.notNull(recruiter.getAddressBook(), "Recruiter address book doesn't exist");
        String email = recruiter.getAddressBook().getEmail();

        if (email == null || email.isEmpty()) {
            throw new CommissionTrackException(String.format("Error resolving email address for recruiter: %s ", recruiter.toString()));
        }

        String[] parts = email.split("@");

        if (this.recruiterRepository.findByRecruiterCode(parts[0]).isPresent() && parts.length >= 2) {
            String[] domainParts = parts[1].split("\\.");
            return parts[0] + "-" + domainParts[0];
        }
        return parts[0];
    }
}
