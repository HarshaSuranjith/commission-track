package lk.tech99x.assessment.commisiontracker.service.business;

import lk.tech99x.assessment.commisiontracker.domain.commission.CommissionViewDTO;
import lk.tech99x.assessment.commisiontracker.domain.commission.MonthlyCommission;
import lk.tech99x.assessment.commisiontracker.service.business.exceptions.CommissionTrackException;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.CommissionViewRepository;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.MonthlyCommissionRepository;
import lk.tech99x.assessment.commisiontracker.service.dataaccess.RecruitmentRequestRepository;
import lk.tech99x.assessment.commisiontracker.service.factory.AbstractObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@ManagedResource
public class RecruiterCommissionAggregatorService {


    private CommissionViewRepository commissionViewRepository;
    private MonthlyCommissionRepository monthlyCommissionRepository;

    @Autowired
    public RecruiterCommissionAggregatorService(CommissionViewRepository commissionViewRepository,
                                                MonthlyCommissionRepository monthlyCommissionRepository) {
        this.commissionViewRepository = commissionViewRepository;
        this.monthlyCommissionRepository = monthlyCommissionRepository;
    }


    /**
     * @TODO: Main operation for creating monthly commissions for head hunters
     */
    @Scheduled(cron = "0 0 0 1 1/1 ?")
    @ManagedOperation
    public void processMonthlyCommission() {

        Set<MonthlyCommission> monthlyCommissions = new HashSet<>();

        LocalDateTime from = LocalDateTime.now().minusMonths(1);
        LocalDateTime to = LocalDateTime.now();

        Set<CommissionViewDTO> viewDTOSet = this.commissionViewRepository.findAllBySubmissionDateBetween(from, to);
        try {
            Map<Integer, List<CommissionViewDTO>> commissionsByRecruiter =
                viewDTOSet.stream().collect(
                    Collectors.groupingBy(CommissionViewDTO::getRecruiterId)
                );

            for (Map.Entry e : commissionsByRecruiter.entrySet()) {
                Integer recruiterId = (Integer) e.getKey();

                MonthlyCommission monthlyCommission = MonthlyCommission.builder()
                    .commission(AbstractObjectFactory.getDefaultMonetaryAmount())
                    .bonus(AbstractObjectFactory.getDefaultMonetaryAmount())
                    .month(YearMonth.now().minusMonths(1))
                    .recruiterId(recruiterId).build();

                for (CommissionViewDTO commission : (List<CommissionViewDTO>) e.getValue()) {
                    monthlyCommission.add(commission.getMasonCommission(), commission.getMasonBonus());
                    monthlyCommission.add(commission.getCarpenterCommission(), commission.getCarpenterBonus());
                }
                monthlyCommission.setIsFinalized(true);
                monthlyCommissions.add(monthlyCommission);
            }
            this.monthlyCommissionRepository.saveAll(monthlyCommissions);
        } catch (Exception e) {
            throw new CommissionTrackException("Error processing monthly commission", e);
        }

    }

}
