package lk.tech99x.assessment.commisiontracker.service.business;

import lk.tech99x.assessment.commisiontracker.domain.commission.CommissionDTO;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruiter;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRecordDTO;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitmentRequest;
import org.springframework.stereotype.Service;

import java.time.Month;
import java.util.Set;

@Service
public interface RecruitersManagementService {
    Set<Recruiter> findAll();

    Recruiter findByRecruiterCode(String recruiterCode);

//    RecruitmentRequest processRecruitmentRequest(RecruitmentRequest request);

    RecruitmentRecordDTO getRecruitmentRecordForMonth(String recruiterCode, Month month);

    RecruitmentRequest addRecruitmentRequest(RecruitmentRequest recruitmentRequest);

    Recruiter addRecruiter(Recruiter recruiter);

    Integer getIdByRecruiterCode(String recruiterCode);

    CommissionDTO getCommissionDetailsForMonth(String recruiterCode, Month month);
}
