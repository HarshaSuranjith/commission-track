package lk.tech99x.assessment.commisiontracker.service.dataaccess;

import lk.tech99x.assessment.commisiontracker.domain.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{
}
