package lk.tech99x.assessment.commisiontracker.util;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

@Converter
public class MonetaryAmountConverter implements AttributeConverter<MonetaryAmount, BigDecimal> {

    @Override
    public BigDecimal convertToDatabaseColumn(MonetaryAmount fromObject) {
        return new BigDecimal(fromObject.getNumber().toString());
    }

    @Override
    public MonetaryAmount convertToEntityAttribute(BigDecimal fromDbColumn) {
        CurrencyUnit usd = Monetary.getCurrency("USD");
        return Monetary.getDefaultAmountFactory()
            .setCurrency(usd).setNumber(fromDbColumn).create();
    }


}
