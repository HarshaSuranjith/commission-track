package lk.tech99x.assessment.commisiontracker.util;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class DurationUtil {

    public static final String START_OF_MONTH = "startOfMoNth";
    public static final String END_OF_MONTH = "startOfMoNth";

    public static Map<String, LocalDateTime> getLastMonthDurations() {
        Map<String, LocalDateTime> map = new HashMap<>();
        LocalDateTime startOfMonth = LocalDateTime.now().withHour(0).withMinute(0).withSecond(1).withNano(1).withDayOfMonth(1);
        LocalDateTime endOfMonth = startOfMonth.withMonth(startOfMonth.getMonth().getValue() + 1).minusNanos(1);

        map.put(START_OF_MONTH, startOfMonth);
        map.put(END_OF_MONTH, endOfMonth);

        return map;
    }

}
