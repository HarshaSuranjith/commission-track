package lk.tech99x.assessment.commisiontracker.util;

import lk.tech99x.assessment.commisiontracker.domain.Constants;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.Recruit;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.RecruitGroups;
import lk.tech99x.assessment.commisiontracker.domain.recruitment.SkillsGroup;
import lk.tech99x.assessment.commisiontracker.service.business.exceptions.CommissionTrackException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class Partitioner {

    public Set<RecruitGroups> partitionGroups(List<Recruit> recruits) {

        Map<SkillsGroup, List<Recruit>> filtered =
            recruits.stream().collect(
                Collectors.groupingBy(Recruit::getSkillsGroup)
            );

        Set<RecruitGroups> allGroups = new HashSet<>();

        for (Map.Entry<SkillsGroup, List<Recruit>> e : filtered.entrySet()) {
            allGroups.addAll(segmentGroups(e.getValue(), e.getKey()));
        }

        return allGroups;

    }

    private Set<RecruitGroups> getAvailableCarpenterGroups(List<Recruit> allCarpenters) {
        return this.segmentGroups(allCarpenters, SkillsGroup.CARPENTRY);
    }

    private Set<RecruitGroups> getAvailableMasonGroups(List<Recruit> allMasons) {
        return this.segmentGroups(allMasons, SkillsGroup.MASONRY);
    }

    private Set<RecruitGroups> segmentGroups(List<Recruit> recruits, SkillsGroup skillsGroup) {
        Set<RecruitGroups> recruitGroups = new HashSet<>();

        int numberOfGroups = recruits.size() / Constants.GROUP_SIZE;

        try {
            int i = 0;
            while (i < numberOfGroups * Constants.GROUP_SIZE - 1) {
                recruitGroups.add(
                    RecruitGroups.builder()
                        .recruits(new HashSet<>(recruits.subList(i, i + Constants.GROUP_SIZE - 1)))
                        .skillsGroup(skillsGroup)
                        .build()
                );
                i = i + Constants.GROUP_SIZE;
            }
        } catch (Exception e) {
            throw new CommissionTrackException("Error segmenting recruit groups", e);
        }
        return recruitGroups;
    }

}
