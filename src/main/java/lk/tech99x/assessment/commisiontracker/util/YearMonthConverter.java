package lk.tech99x.assessment.commisiontracker.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.YearMonth;

@Converter
public class YearMonthConverter implements AttributeConverter<YearMonth, String> {
    @Override
    public String convertToDatabaseColumn(YearMonth yearMonth) {
        return yearMonth.toString();
    }

    @Override
    public YearMonth convertToEntityAttribute(String dbYearMonth) {
        String[] parts = dbYearMonth.split("-");
        return YearMonth.of(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
    }
}
