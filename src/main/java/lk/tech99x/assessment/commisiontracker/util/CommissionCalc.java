package lk.tech99x.assessment.commisiontracker.util;

import lk.tech99x.assessment.commisiontracker.domain.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;

@Component
@PropertySource("classpath:commission.properties")
public class CommissionCalc {

    private static BigDecimal commissionPerMason;
    private static BigDecimal commissionPerCarpenter;
    private static BigDecimal bonusPercentage;


    public static MonetaryAmount getMasonCommission(Integer masonsCount) {
        return calculateAmount(masonsCount, commissionPerMason);
    }

    public static MonetaryAmount getCarpenterCommission(Integer carpenterCount) {
        return calculateAmount(carpenterCount, commissionPerCarpenter);
    }

    public static MonetaryAmount getMasonBonus(Integer masonGroupsCount) {
        return calculateAmount(masonGroupsCount * Constants.GROUP_SIZE, commissionPerMason).multiply(bonusPercentage).multiply(0.01f);
    }

    public static MonetaryAmount getCarpenterBonus(Integer carpenterGroupCount) {
        return calculateAmount(carpenterGroupCount * Constants.GROUP_SIZE, commissionPerCarpenter).multiply(bonusPercentage).multiply(0.01f);
    }

    private static MonetaryAmount calculateAmount(Integer count, BigDecimal rate) {
        return Monetary.getDefaultAmountFactory()
            .setCurrency(Constants.DEFAULT_CURRENCY_UNIT)
            .setNumber(rate.multiply(new BigDecimal(count)))
            .create();
    }

    @Value("${commission.mason}")
    public void setCommissionPerMason(BigDecimal commissionPerMason) {
        CommissionCalc.commissionPerMason = commissionPerMason;
    }

    @Value("${commission.carpenter}")
    public void setCommissionPerCarpenter(BigDecimal commissionPerCarpenter) {
        CommissionCalc.commissionPerCarpenter = commissionPerCarpenter;
    }

    @Value("${commission.bonus-percent}")
    public void setBonusPercentage(BigDecimal bonusPercentage) {
        CommissionCalc.bonusPercentage = bonusPercentage;
    }

}
